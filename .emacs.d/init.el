;; Install straight.el
(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 6))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
	(url-retrieve-synchronously
	 "https://raw.githubusercontent.com/radian-software/straight.el/develop/install.el"
	 'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

;; install use-package
(straight-use-package 'use-package)

;; configure straight to use straight.el by default
(use-package straight
  :custom
(straight-use-package-by-default t))

(use-package beacon)

(use-package doom-themes)

(use-package toc-org
   :commands toc-org-enable
   :init (add-hook 'org-mode-hook 'toc-org-enable))

(use-package org-bullets)
 (add-hook 'org-mode-hook (lambda () (org-bullets-mode 1)))

(use-package org-tempo
 :straight nil)

(use-package focus)

(use-package which-key
  :straight t)
(which-key-mode 1)

(use-package doom-modeline
  :straight t
  :init (doom-modeline-mode 1))

(use-package all-the-icons
      :if (display-graphic-p))

(use-package org-auto-tangle
:defer t
:hook (org-mode . org-auto-tangle-mode))

;;install vertico
(use-package vertico
  :init
  (vertico-mode)

  ;; Different scroll margin
  ;; (setq vertico-scroll-margin 0)

  ;; Show more candidates
  (setq vertico-count 20)

  ;; Grow and shrink the Vertico minibuffer
  (setq vertico-resize t)

  ;; Optionally enable cycling for `vertico-next' and `vertico-previous'.
  (setq vertico-cycle t))

(use-package vertico-directory
:after vertico
:straight nil
:load-path "straight/repos/vertico/extensions/"
:bind (:map vertico-map
            ("RET" . vertico-directory-enter)
            ("DEL" . vertico-directory-delete-char)
            ("M-DEL" . vertico-directory-delete-word))

:hook (rfn-eshadow-update-overlay . vertico-directory-tidy))

(use-package corfu
  :custom
  (corfu-cycle t)
 :hook (prog-mode . corfu-mode)
 :init
(global-corfu-mode))

;; Enable rich annotations using the Marginalia package
(use-package marginalia
  ;; Bind `marginalia-cycle' locally in the minibuffer.  To make the binding
  ;; available in the *Completions* buffer, add it to the
  ;; `completion-list-mode-map'.
  :bind (:map minibuffer-local-map
         ("M-A" . marginalia-cycle))

  ;; The :init section is always executed.
  :init

  ;; Marginalia must be actived in the :init section of use-package such that
  ;; the mode gets enabled right away. Note that this forces loading the
  ;; package.
  (marginalia-mode))

(use-package projectile)

(use-package evil
  :straight t
  :init
  (setq evil-want-keybinding nil)
  :config
  (evil-mode 1))

(use-package evil-collection
  :after evil
  :straight t
  :config
  (evil-collection-init))

;;install dashboard
(use-package dashboard
   :straight t
   :init
 (setq dashboard-set-heading-icons t)
 (setq dashboard-set-file-icons t)
 (setq dashboard-banner-logo-title "emacs rocks")
 (setq dashboard-startup-banner 'logo)
 (setq dashboard-center-content nil)
 (setq dashboard-items '((recents . 5)
                         (projects . 3)
                         (agenda . 3))))
                        
 :config
(dashboard-setup-startup-hook)

;;install general
(use-package general
  :config
  (general-evil-setup t))

(use-package perspective
  :bind
  ("C-x C-b" . persp-list-buffers)         ; or use a nicer switcher, see below
  :custom
  (persp-mode-prefix-key (kbd "C-c M-p"))  ; pick your own prefix key here
  :init
  (persp-mode))

(use-package rust-mode)

(use-package haskell-mode)

(use-package eglot
    :hook
    (c-mode . eglot-ensure)
    (python-mode . eglot-ensure)
    (rust-mode . eglot-ensure))

;; Using garbage magic hack.
 (use-package gcmh
   :config
   (gcmh-mode 1))
;; Setting garbage collection threshold
(setq gc-cons-threshold 402653184
      gc-cons-percentage 0.6)

;; Profile emacs startup
(add-hook 'emacs-startup-hook
          (lambda ()
            (message "*** Emacs loaded in %s with %d garbage collections."
                     (format "%.2f seconds"
                             (float-time
                              (time-subtract after-init-time before-init-time)))
                     gcs-done)))

;; Silence compiler warnings as they can be pretty disruptive (setq comp-async-report-warnings-errors nil)

;; Silence compiler warnings as they can be pretty disruptive
(if (boundp 'comp-deferred-compilation)
    (setq comp-deferred-compilation nil)
    (setq native-comp-deferred-compilation nil))
;; In noninteractive sessions, prioritize non-byte-compiled source files to
;; prevent the use of stale byte-code. Otherwise, it saves us a little IO time
;; to skip the mtime checks on every *.elc file.
(setq load-prefer-newer noninteractive)

(add-hook 'org-mode-hook 'org-indent-mode)
(setq org-directory "~/Org/"
      org-agenda-files '("~/Org/agenda.org")
      org-default-notes-file (expand-file-name "notes.org" org-directory)
      org-ellipsis " ▼ "
      org-log-done 'time
      org-journal-dir "~/Org/journal/"
      org-journal-date-format "%B %d, %Y (%A) "
      org-journal-file-format "%Y-%m-%d.org"
      org-hide-emphasis-markers t)
(setq org-src-preserve-indentation nil
      org-src-tab-acts-natively t
      org-edit-src-content-indentation 0
      org-src-fontify-natively t
      org-confirm-babel-evaluate nil)

;; load dashboard instead of scratchpad at startup *INSTALL DASHBOARD*
(setq initial-buffer-choice (lambda () (get-buffer "*dashboard*")))
(load-theme 'doom-dracula t) ; Set theme
(menu-bar-mode -1) ; Turn menubar off
(scroll-bar-mode -1) ; Turn scrollbar off
(tool-bar-mode -1) ; Turn tool-bar off
(global-display-line-numbers-mode 1) ; Display line numbers globally
(beacon-mode 1) ; Turn beacon on
(setq inhibit-startup-screen t) ; Disable startup default startup screen
(setq auto-save-interval 1000)
(setq make-backup-files nil) ; Disable automatic backup files

(set-face-attribute 'default nil 
  :font "SauceCodePro Nerd Font"
  :height 120
  :weight 'semibold)
(set-face-attribute 'variable-pitch nil 
  :font "SauceCodePro Nerd Font"
  :height 120
  :weight 'semibold)
(set-face-attribute 'fixed-pitch nil
  :font "JetBrainsMono Nerd Font"
  :height 120
  :weight 'semibold)
;; Makes commented text and keywords italics.
;; This is working in emacsclient but not emacs.
;; Your font must have an italic face available.
(set-face-attribute 'font-lock-comment-face nil
  :slant 'italic)
(set-face-attribute 'font-lock-keyword-face nil
  :slant 'italic)

;; Uncomment the following line if line spacing needs adjusting.
;;(setq-default line-spacing 0.12)

;; Needed if using emacsclient. Otherwise, your fonts will be smaller than expected.
;;(add-to-list 'default-frame-alist '(font . "SauceCodePro Nerd Font-16" :weight 'semibold))
;; changes certain keywords to symbols, such as lamda!
(setq global-prettify-symbols-mode t)
;;(add-hook 'find-file-hook (lambda () (set-face-attribute 'default nil :height 105)))

(nvmap :keymaps 'override :prefix "SPC"
  "SPC"   '(execute-extended-command :which-key "M-x") ;; using consult instead of counsel
)

;; Emacs Specific Keybinds
(nvmap :prefix "SPC"
  "h"     '(:ignore t :wk "help")
  "h r v" '(describe-variable :which-key "describe variable")
  "h r f" '(describe-function :which-key "describe function")
)
 ;; General Keybinds          
(nvmap :prefix "SPC"
  "f f"  '(find-file :which-key "find file") ;; find-file using dired
  "b i"  '(persp-ibuffer :which-key "perspective-ibuffer") ;; using perspective-ibuffer instead of ibuffer
  "p"    '(:ignore t :wk "perspective")
  "p s"  '(persp-switch :which-key "perspective-switch") ;; this adds a workspace or shows all workspaces avaible
  "p p"  '(persp-prev :which-key "previous perspective") ;; previous workspace
  "p n"  '(persp-next :which-key "next perspective") ;; next workspace
  "p k"  '(persp-kill :which-key "kill perspective") ;; kill the current perspective
  "p K"  '(persp-kill-others :which-key "kill other perspectives") ;; kills all other perspectives except current one
  "p r"  '(persp-remove :which-key "remove buffer from perspective") ;; removes a buffer from current perspective
  "p a"  '(persp-add-buffer :which-key "add buffer to current perspective") ;; copies one buffer to current perspective, keeping orginal *NOTE* killing buffer will remove it from orginal
  "p S"  '(persp-set-buffer :which-key "add buffer to curreent perspective and kill orginal") ;; copies one buffer to current perspective BUT removes it from orignal
)
