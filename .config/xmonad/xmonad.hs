-- XMonad --
import XMonad
import XMonad.Operations
import System.IO
import System.Exit (exitSuccess)

-- Data --
import Data.Monoid
import Data.Maybe(isJust)

-- Actions --
import XMonad.Actions.CopyWindow (kill1, killAllOtherCopies)
import XMonad.Actions.WithAll (sinkAll, killAll)
import XMonad.Actions.CycleWS (moveTo, shiftTo, WSType(..), nextScreen, prevScreen)

-- Hooks --
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.WorkspaceHistory
import XMonad.Hooks.FadeInactive
import XMonad.Hooks.DynamicLog (dynamicLogWithPP, wrap, xmobarPP, xmobarColor, shorten, PP(..))
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.ManageHelpers (isFullscreen, doFullFloat, doCenterFloat)
import XMonad.Hooks.SetWMName
import XMonad.Hooks.StatusBar.PP

-- Layouts --
import XMonad.Layout.NoBorders
import XMonad.Layout.Spacing
import XMonad.Layout.LayoutModifier
import XMonad.Layout.SimplestFloat
import XMonad.Layout.ResizableTile
import XMonad.Layout.WindowArranger (windowArrange, WindowArrangerMsg(..))
import XMonad.Layout.Renamed (renamed, Rename(Replace))
import XMonad.Layout.LimitWindows (limitWindows, increaseLimit, decreaseLimit)
import XMonad.Layout.MultiToggle (mkToggle, single, EOT(EOT), (??))
import XMonad.Layout.MultiToggle.Instances (StdTransformers(NBFULL, MIRROR, NOBORDERS))
import XMonad.Layout.MultiToggle as MT (Toggle(..))

-- Utilities --
import XMonad.Util.SpawnOnce
import XMonad.Util.Run(spawnPipe)
import XMonad.Util.EZConfig (additionalKeysP, mkKeymap)
import XMonad.Util.NamedScratchpad

-- Qualified -- 
import qualified Data.Map as M
import qualified XMonad.StackSet as W
import qualified XMonad.Layout.ToggleLayouts as T (toggleLayouts, ToggleLayout(Toggle))

myTerminal :: String
myTerminal = "kitty"
myModMask  = mod4Mask


myBorderWidth :: Dimension
myBorderWidth = 2
myWorkspaces :: [String]
myWorkspaces = ["1","2","3","4","5","6","7","8","9"]

myStartupHook :: X ()
myStartupHook = do
             spawnOnce "nitrogen --restore"
             spawnOnce "picom --vsync"
             spawn "emacs --with-x-toolkit=lucid --daemon"
             setWMName "LG3D"

mySpacing :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing i = spacingRaw False (Border i i i i) True  (Border i i i i) True

tall     = renamed [Replace "tall"]
           $ mySpacing 8
           $ ResizableTall 1 (3/100) (1/2) []

monocle  = renamed [Replace "monocle"]
           $ limitWindows 20 Full

floats   = renamed [Replace "floats"]
           $ limitWindows 20 simplestFloat

myLogHook :: X ()
myLogHook = fadeInactiveLogHook fadeAmount
      where fadeAmount = 1.0

myLayoutHook = avoidStruts $ windowArrange $ T.toggleLayouts floats $
               mkToggle (NBFULL ?? NOBORDERS ?? EOT) myDefaultLayout
              where
              myDefaultLayout =  tall
                              ||| noBorders monocle
                              ||| floats

myScratchPads = [
-- run htop in xterm, find it by title, use default floating window placement
    NS "htop" "xterm -e htop" (title =? "htop") defaultFloating ,

-- run stardict, find it by class name, place it in the floating window
-- 1/6 of screen width from the left, 1/6 of screen height
-- from the top, 2/3 of screen width by 2/3 of screen height
    NS "stardict" "stardict" (className =? "Stardict")
        (customFloating $ W.RationalRect (1/6) (1/6) (2/3) (2/3)) ,

-- run gvim, find by role, don't float
    NS "notes" "gvim --role notes ~/notes.txt" (role =? "notes") nonFloating
                ]
                 where role = stringProperty "WM_WINDOW_ROLE"

myManageHook :: XMonad.Query (Data.Monoid.Endo WindowSet)

myManageHook = composeAll
     
  [ className =? "confirm"         --> doFloat
  , className =? "file_progress"   --> doFloat
  , className =? "dialog"          --> doFloat
  , className =? "download"        --> doFloat
  , className =? "error"           --> doFloat
  , className =? "Gimp"            --> doFloat
  , className =? "notification"    --> doFloat
  , className =? "pinentry-gtk-2"  --> doFloat
  , className =? "splash"          --> doFloat
  , className =? "toolbar"         --> doFloat
  , className =? "Yad"             --> doCenterFloat
  , title =? "Oracle VM VirtualBox Manager"   --> doFloat
  , title =? "Order Chain - Market Snapshots" --> doFloat
  , title =? "Mozilla Firefox"     --> doShift ( myWorkspaces !! 1 )
  , className =? "Brave-browser"   --> doShift ( myWorkspaces !! 1 )
  , className =? "mpv"             --> doShift ( myWorkspaces !! 7 )
  , className =? "Gimp"            --> doShift ( myWorkspaces !! 8 )
  , className =? "VirtualBox Manager" --> doShift  ( myWorkspaces !! 4 )
  , (className =? "firefox" <&&> resource =? "Dialog") --> doFloat  -- Float Firefox Dialog
  , isFullscreen -->  doFullFloat
  ]

myKeys :: [(String, X ())]
myKeys = 
       -- Applications --
    [ ("M-<Return>", spawn myTerminal),
      ("M-p", spawn "dmenu_run"),
      ("M-s", spawn "flameshot gui"),
      
      -- Browsers --
      ("M-b f", spawn "firefox"),
      ("M-b b", spawn "brave"),
      ("M-b q", spawn "qutebrowser"),

      -- Scratchpads --
      ("M-t", namedScratchpadAction myScratchPads "htop"),
      ("M-S-t",  namedScratchpadAction myScratchPads "mocp"),
      -- Emacs --
      ("M-e e", spawn "emacsclient -c -a 'emacs'"),
      ("M-e d", spawn "emacsclient -c -a '' --eval '(dired nil)'"),

      -- Layouts --
      ("M-<Tab>", sendMessage NextLayout),
      ("M-<Space>", sendMessage (MT.Toggle NBFULL) >> sendMessage ToggleStruts),
      ("M-f", sendMessage (T.Toggle "floats")),
      ("M-S-<Delete>", sinkAll),
      -- Xmonad --
      ("M-S-c", kill1),
      ("M-S-r", spawn "xmonad --restart"),
      ("M-q", io exitSuccess)
    ]
    
        where nonNSP = WSIs (return (\ws -> W.tag ws /= "nsp"))
              nonEmptyNonNSP  = WSIs (return (\ws -> isJust (W.stack ws) && W.tag ws /= "nsp"))

main :: IO ()
main = do
       -- launch Xmobar
       xmproc <- spawnPipe "xmobar -x 0 ~/.config/xmobar/xmobarrc"
       xmproc1 <- spawnPipe "xmobar -x 1 ~/.config/xmobar/xmobarrc"
       xmonad $ docks $ ewmh $ def
--{ manageHook = ( isFullscreen --> doFullFloat ) <+> myManageHook <+> manageDocks,
         { manageHook = myManageHook <+> manageDocks <+> namedScratchpadManageHook myScratchPads,
          terminal        = myTerminal,
          modMask         = myModMask,
         -- keys            = myKeys,
          workspaces      = myWorkspaces,
          layoutHook      = myLayoutHook,
          borderWidth     = myBorderWidth,
          startupHook     = myStartupHook,
         -- logHook = workspaceHistoryHook <+> myLogHook <+> dynamicLogWithPP xmobarPP
            logHook = dynamicLogWithPP $ filterOutWsPP [scratchpadWorkspaceTag] $ xmobarPP
                        { ppOutput = \x -> hPutStrLn xmproc x >> hPutStrLn xmproc1 x,
                          ppCurrent = xmobarColor "#c3e88d" "" . wrap "[" "]",
                          ppVisible = xmobarColor "#c3e88d" "",
                          ppHidden = xmobarColor "#82AAFF" "" . wrap "*" "",
                          ppHiddenNoWindows = xmobarColor "#F07178" "",
                          ppTitle = xmobarColor "#d0d0d0" "" . shorten 60,
                          ppUrgent = xmobarColor "#C45500" "" . wrap "!" "!",
                          ppSep = "<fc=#666666> | </fc>"
                        }
             
         } `additionalKeysP` myKeys
