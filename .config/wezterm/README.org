#+title: Wezterm Config File
#+PROPERTY: header-args :tangle wezterm.lua
#+AUTHOR: Chris Smith


* Table of Contents :toc:
- [[#fonts-and-themes][Fonts and Themes]]
- [[#config][Config]]

* Fonts and Themes
#+begin_src lua
local wezterm = require 'wezterm'
local config = {}
config.color_scheme = 'Dracula' -- set default color scheme
config.font_size = 16 -- set default font size
config.font =
  wezterm.font('JetBrainsMono Nerd Font', {weight = 'Bold', italic = false}) -- set default font

#+end_src

* Config
#+begin_src lua
config.default_prog = { '/bin/fish' } -- set default shell to fish
config.hide_tab_bar_if_only_one_tab = true -- hide the tab bar if only one tab is open
config.hide_mouse_cursor_when_typing = true -- hide mouse cursor when typing in the panel
config.default_cursor_style = 'SteadyUnderline' -- default cursor style is underline

return config

#+end_src
