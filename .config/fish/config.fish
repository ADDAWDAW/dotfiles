set -g fish_prompt_pwd_dir_length 0
set -g fish_greeting ''

alias ls="$HOME/.cargo/bin/exa -lh --color always --group-directories-first --icons"
#alias ls="/usr/bin/lsd -lh --color always --group-directories-first"
alias cat="/usr/bin/bat"
alias bare="/usr/bin/git --git-dir=$HOME/.config/dotfiles.git --work-tree=$HOME"
if status is-interactive
    # Commands to run in interactive sessions can go here
end

neofetch
starship init fish | source
